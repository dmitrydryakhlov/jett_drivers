import React from 'react';
import { iGPSSmallTypeTracker, iGPSTypeTracker, iTrackerTypeModal } from '../models/models';
import { changeTrackerType } from '../actions/trackersTypeActions';
import { connect } from 'react-redux';
import { changeTrackersTypeModal } from '../actions/modalsActions';

interface iTTModalProps {
    trackersTypeDetail: iGPSTypeTracker
    trackersTypeModal: iTrackerTypeModal
    changeTrackerType: Function
    changeTrackersTypeModal: Function
}

class TrackerTypeInfoModal extends React.Component<iTTModalProps> {

    onNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.props.changeTrackersTypeModal({
            name: event.currentTarget.value,
            recurringPaymentSmsBlocked: this.props.trackersTypeModal.recurringPaymentSmsBlocked,
            recurringPaymentSmsUnblocked: this.props.trackersTypeModal.recurringPaymentSmsUnblocked
        });
    }
    onSmsBlockedChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.props.changeTrackersTypeModal({
            name: this.props.trackersTypeModal.name,
            recurringPaymentSmsBlocked: event.currentTarget.value,
            recurringPaymentSmsUnblocked: this.props.trackersTypeModal.recurringPaymentSmsUnblocked
        });
    }
    onSmsUnblockedChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.props.changeTrackersTypeModal({
            name: this.props.trackersTypeModal.name,
            recurringPaymentSmsBlocked: this.props.trackersTypeModal.recurringPaymentSmsBlocked,
            recurringPaymentSmsUnblocked: event.currentTarget.value
        });
    }
    changeTrackerType = (event: React.MouseEvent<HTMLElement>) => {
        const { name, recurringPaymentSmsBlocked, recurringPaymentSmsUnblocked } = this.props.trackersTypeModal
        this.props.changeTrackerType(
            this.props.trackersTypeDetail.id,
            name,
            recurringPaymentSmsBlocked,
            recurringPaymentSmsUnblocked
        )
    }
    render() {
        return (
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">Информация о типе GPS трекера</h5>
                    </div>
                    <div className="modal-body">
                        <form>
                            <div className="form-group">
                                <label htmlFor="trackersTypeName">Название</label>
                                <input type="text" className="form-control" id="trackersTypeName"
                                    value={this.props.trackersTypeModal.name}
                                    onChange={this.onNameChange}
                                    placeholder={this.props.trackersTypeDetail.name} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="trackersTypeSMSBlock">Сообщение SMS при блокировке</label>
                                <input type="text" className="form-control" id="trackersTypeSMSBlock"
                                    value={this.props.trackersTypeModal.recurringPaymentSmsBlocked}
                                    onChange={this.onSmsBlockedChange}
                                    placeholder={this.props.trackersTypeDetail.recurringPaymentSmsBlocked} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="trackersTypeSMSUnblock">Сообщение SMS при снятии блокировки</label>
                                <input type="text" className="form-control" id="trackersTypeSMSUnblock"
                                    value={this.props.trackersTypeModal.recurringPaymentSmsUnblocked}
                                    onChange={this.onSmsUnblockedChange}
                                    placeholder={this.props.trackersTypeDetail.recurringPaymentSmsUnblocked} />
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                                <button type="button" className="btn btn-primary" onClick={this.changeTrackerType}>Сохранить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (store: any) => {
    return {
        trackersTypeDetail: store.trackersType.trackersTypeDetail,
        trackersTypeModal: store.modals.trackersTypeModal
    }
}

const mapDispatchToProps = (dispatch: any) => ({
    changeTrackerType: (
        id: string,
        name: string,
        recurringPaymentSmsBlocked: string,
        recurringPaymentSmsUnblocked: string
    ) => dispatch(changeTrackerType(id, name, recurringPaymentSmsBlocked, recurringPaymentSmsUnblocked)),
    changeTrackersTypeModal: (modalsData: iTrackerTypeModal) =>
        dispatch(changeTrackersTypeModal(modalsData)),
})

export default connect(mapStateToProps, mapDispatchToProps)(TrackerTypeInfoModal);

