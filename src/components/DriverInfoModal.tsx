import React from 'react';
import { connect } from 'react-redux';
import { iDriver, iHistory, iPagintion } from '../models/models';
import Pagination from './Pagination';
import { changeDriversModal } from '../actions/modalsActions';
import { changeDriver } from '../actions/driversActions';

interface iProps {
    driversDetail: iDriver
    balanceHistories: iHistory[]
    changeBalance: Function
    pagination: iPagintion
    changePage: Function
    changeDriversModal: Function
    changeDriver: Function
    driversModal: iDriver
}

class DriverInfoModal extends React.Component<iProps> {

    state = {
        changeBalance: undefined,
        comment: undefined
    }
    onBalanceChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ ...this.state, changeBalance: +event.currentTarget.value });
    }
    onCommentChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ ...this.state, comment: event.currentTarget.value });
    }

    onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { id, value } = event.currentTarget
        this.props.changeDriversModal({ ...this.props.driversModal, [id]: value });
    }

    onCarChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { id, value } = event.currentTarget
        const newCar = { ...this.props.driversModal.car, [id]: value }
        this.props.changeDriversModal({ ...this.props.driversModal, car: newCar });
    }

    onSelectChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const { id, value } = event.currentTarget
        this.props.changeDriversModal({ ...this.props.driversModal, [id]: value });
    }

    changeDriver = (event: React.MouseEvent<HTMLElement>) => {
        this.props.changeDriver(this.props.driversDetail.id, this.props.driversModal);
    }

    handlerBalanceChange = (event: React.MouseEvent<HTMLElement>) => {
        this.props.changeBalance(
            this.props.driversDetail.id,
            this.state.changeBalance,
            this.state.comment
        );
    }
    render() {
        const { driversModal } = this.props;
        return (
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">Информация о водителе</h5>
                    </div>
                    <div className="modal-body">
                        <form>
                            <div className="form-group">
                                <label htmlFor="name">Имя</label>
                                <input type="text" className="form-control" id="name"
                                    value={driversModal.name} onChange={this.onInputChange} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="driverPhone">Телефон</label>
                                <input type="text" className="form-control" id="phone"
                                    value={driversModal.phone} onChange={this.onInputChange} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="balance">Баланс</label>
                                <input type="text" className="form-control" id="balance" disabled
                                    value={driversModal.balance} onChange={this.onInputChange} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="driverChangeBalance">Изменение баланса</label>
                                <input type="text" className="form-control" id="driverChangeBalance"
                                    placeholder="Отрицательные значения спишут сумму со счёта"
                                    onChange={this.onBalanceChange} />
                                <input type="text" className="form-control" id="driverChangeBalanceComment"
                                    placeholder="Комментарий"
                                    onChange={this.onCommentChange} />
                                <button type="button" className="btn btn-secondary"
                                    onClick={this.handlerBalanceChange}>Пополнить</button>
                            </div>
                            <div className="form-group">
                                <label htmlFor="city">Город</label>
                                <input type="text" className="form-control" id="city"
                                    value={driversModal.city} onChange={this.onInputChange} />
                            </div>

                            <label htmlFor="historyTable">История пополнений и списаний </label>
                            <table className="table table-striped" id="historyTable">
                                <thead>
                                    <tr>
                                        <td>Сумма</td>
                                        <td>Комментарий</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.props.balanceHistories.map((item, index) =>
                                        <tr key={index}>
                                            <td>{item.amount}</td>
                                            <td>{item.comment}</td>
                                        </tr>
                                    )}
                                </tbody>
                            </table>
                            <Pagination
                                Pagination={this.props.pagination}
                                changePage={this.props.changePage}
                                id={this.props.driversDetail.id} />

                            <div className="form-row">
                                <div className="form-group col-md-3">
                                    <label htmlFor="model">Модель</label>
                                    <input type="text" className="form-control" id="model"
                                        value={driversModal.car && driversModal.car.model}
                                        onChange={this.onCarChange}
                                    />
                                </div>
                                <div className="form-group col-md-3">
                                    <label htmlFor="mark">Марка</label>
                                    <input type="text" className="form-control" id="mark"
                                        value={driversModal.car && driversModal.car.mark}
                                        onChange={this.onCarChange}
                                    />
                                </div>
                                <div className="form-group col-md-3">
                                    <label htmlFor="color">Цвет</label>
                                    <input type="text" className="form-control" id="color"
                                        value={driversModal.car && driversModal.car.color}
                                        onChange={this.onCarChange}
                                    />
                                </div>
                                <div className="form-group col-md-3">
                                    <label htmlFor="number">Номер</label>
                                    <input type="text" className="form-control" id="number"
                                        value={driversModal.car && driversModal.car.number}
                                        onChange={this.onCarChange}
                                    />
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                                <button type="button" className="btn btn-primary"
                                    onClick={this.changeDriver}>Сохранить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (store: any) => {
    return {
        driversDetail: store.drivers.driversDetail,
        driversModal: store.modals.driversModal,
    }
}

const mapDispatchToProps = (dispatch: any) => ({
    changeDriversModal: (modalsData: iDriver) => dispatch(changeDriversModal(modalsData)),
    changeDriver: (id: string, modalsData: iDriver) => dispatch(changeDriver(id, modalsData))
})

export default connect(mapStateToProps, mapDispatchToProps)(DriverInfoModal);