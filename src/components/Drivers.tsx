import React from 'react';
import { DriversTable } from './DriversTable';
import { iSmallDriver, iDriver, iPagintion } from '../models/models';
import Pagination from './Pagination';
import $ from "jquery";
import { connect } from 'react-redux';
import { getDrivers, createDriver } from '../actions/driversActions';

type MyProps = {
    createDriverAction: Function;
    getDrivers: Function;
    isFetching: boolean;
    driversList: iSmallDriver[];
    pagination: iPagintion;
};
class Drivers extends React.Component<MyProps> {

    componentDidMount() {
        this.props.getDrivers(1, 10);
    }

    state = {
        name: null,
        phone: null,
        city: null,
        promo: null,
        car: {
            model: null,
            mark: null,
            color: null,
            number: null
        },
    }

    onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { id, value } = event.currentTarget;
        this.setState({ ...this.state, [id]: value })
    }
    onCarChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { id, value } = event.currentTarget;
        this.setState({ ...this.state, car: { ...this.state.car, [id]: value } });
    }
    validate() {
        return (this.state.city && this.state.name && this.state.phone && this.state.car.mark &&
            this.state.car.model && this.state.car.number && this.state.car.color
        )
    }

    handlerSubmitBtn = (event: React.MouseEvent<HTMLElement>) => {
        this.props.createDriverAction(this.state);
    }


    render() {
        return (
            <div className="DriversPage">
                <div className="btn-group float-right" role="group">
                    <button type="button" className="btn btn-success" data-toggle="modal" data-target="#addDriverModal">
                        Добавить водителя
                        </button>
                    <div className="modal fade" id="addDriverModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalLabel">Создание водителя</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <form>
                                        <div className="form-group">
                                            <label htmlFor="name">ФИО</label>
                                            <input type="text" className="form-control" id="name"
                                                onChange={this.onInputChange} required />
                                        </div>
                                        <div className="form-row">
                                            <div className="form-group input-group col-md-6">
                                                <div className="input-group mb-3">
                                                    <label htmlFor="phone">Номер телефона</label>
                                                    <div className="input-group-prepend">
                                                        <span className="input-group-text" id="basic-addon1">+7</span>
                                                        <input type="text" className="form-control" id="phone"
                                                            placeholder="номер телефона" aria-label="Username"
                                                            aria-describedby="basic-addon1"
                                                            onChange={this.onInputChange} />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="form-group col-md-6">
                                                <label htmlFor="city">Город</label>
                                                <input type="text" className="form-control" id="city"
                                                    onChange={this.onInputChange} />
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="promo">Промокод</label>
                                            <input type="text" className="form-control" id="promo"
                                                onChange={this.onInputChange} />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="mark">Марка</label>
                                            <input type="text" className="form-control" id="mark"
                                                placeholder="Audi" onChange={this.onCarChange} />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="model">Модель</label>
                                            <input type="text" className="form-control" id="model"
                                                placeholder="a4" onChange={this.onCarChange} />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="color">Цвет</label>
                                            <input type="text" className="form-control" id="color"
                                                placeholder="Черный" onChange={this.onCarChange} />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="number">Номер</label>
                                            <input type="text" className="form-control" id="number"
                                                placeholder="A123BC52" onChange={this.onCarChange} />
                                        </div>
                                        <div className="modal-footer">
                                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                                            <button type="button" onClick={this.handlerSubmitBtn} className="btn btn-primary">Создать</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div >
                </div>
                <DriversTable driversList={this.props.driversList} />
                <Pagination Pagination={this.props.pagination} changePage={this.props.getDrivers} id={0} />
            </div >
        )
    }
}

const mapStateToProps = (store: any) => {
    return {
        driversList: store.drivers.driversList,
        isFetching: store.drivers.isFetching,
        pagination: store.pagination.driversPagination
    }
}

const mapDispatchToProps = (dispatch: any) => ({
    getDrivers: (page: number, limit: number) => dispatch(getDrivers(page, limit)),
    createDriverAction: (driver: iDriver) => dispatch(createDriver(driver)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Drivers);