import React from 'react';
import { iSmallDriver, iDriver, iHistory, iPagintion } from '../models/models';
import DriverInfoModal from './DriverInfoModal';

import { connect } from 'react-redux';
import { getDriversDetail, changeDriversBalance, getDriversHistory } from '../actions/driversActions';

type DriversTableElementProps = {
    iSmallDriver: iSmallDriver,
    driversDetail: iDriver
    getDriversDetail: Function
    changeDriversBalance: Function
    getDriversHistory: Function
    balanceHistories: iHistory[]
    pagination: iPagintion
    //changePage: Function
}

class DriversTableElement extends React.Component<DriversTableElementProps> {

    constructor(arg: Readonly<DriversTableElementProps>) {
        super(arg);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        const { iSmallDriver } = this.props;
        this.props.getDriversDetail(iSmallDriver.id);
        this.props.getDriversHistory(0, 10, iSmallDriver.id);
    }

    render() {
        const { iSmallDriver, driversDetail } = this.props;
        return (
            <tr>
                <td>{iSmallDriver.name}</td>
                <td>{iSmallDriver.city}</td>
                <td>{iSmallDriver.balance}</td>
                <td>+7{iSmallDriver.phone}</td>
                <td>
                    <button type="button" className="btn btn-secondary"
                        data-toggle="modal" data-target="#driversInfoModal"
                        onClick={this.handleClick}>
                        Информация
                    </button>
                    <span className="modal fade" id="driversInfoModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <DriverInfoModal
                            changeBalance={this.props.changeDriversBalance}
                            balanceHistories={this.props.balanceHistories}
                            pagination={this.props.pagination}
                            changePage={this.props.getDriversHistory} />
                    </span>
                </td>
            </tr>
        );
    }
}


const mapStateToProps = (store: any) => {
    return {
        driversDetail: store.drivers.driversDetail,
        balanceHistories: store.drivers.balanceHistories,
        pagination: store.pagination.historyPagination
    }
}

const mapDispatchToProps = (dispatch: any) => ({
    getDriversDetail: (id: number) => dispatch(getDriversDetail(id)),
    getDriversHistory: (page: number, limit: number, id: number) => dispatch(getDriversHistory(page, limit, id)),
    changeDriversBalance: (id: number, amount: number, comment: string) =>
        dispatch(changeDriversBalance(id, amount, comment)),
})

export default connect(mapStateToProps, mapDispatchToProps)(DriversTableElement);