import React from 'react';
import { TrackerTableElement } from './TrackerTableElement';
import { iGPSSmallTracker } from '../models/models';

type trackersTableProps = {
    trackersList: iGPSSmallTracker[];
    changeTracker: Function
    getTrackers: Function
    getTrackerDetail: Function
};
export class TrackersTable extends React.Component<trackersTableProps> {

    componentDidMount() {
        this.props.getTrackers();
    }

    renderRow(items: iGPSSmallTracker[]) {
        let rows = items.map((item, index) => {
            return (
                <TrackerTableElement
                    TrackersTableElementBody={item}
                    changeTracker={this.props.changeTracker}
                    getTrackerDetail={this.props.getTrackerDetail}
                    key={index} />
            )
        })
        return rows;
    }

    render() {
        return (
            <table className="table table-striped ">
                <thead>
                    <tr>
                        <th scope="col">Имя</th>
                        <th scope="col">Баланс водителя</th>
                        <th scope="col">Телефон водителя</th>
                        <th scope="col">Информация</th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderRow(this.props.trackersList)}
                </tbody>
            </table>
        );
    }
}
