import React from 'react';
import { iGPSSmallTracker } from '../models/models';
import TrackerInfoModal from './TrackerInfoModal';

type TrackerTableElementProps = {
    TrackersTableElementBody: iGPSSmallTracker,
    changeTracker: Function,
    getTrackerDetail: Function
}

export class TrackerTableElement extends React.Component<TrackerTableElementProps> {

    constructor(arg: Readonly<TrackerTableElementProps>) {
        super(arg);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        const { id } = this.props.TrackersTableElementBody;
        this.props.getTrackerDetail(id);
    }
    render() {
        return (
            <tr>
                <td>{this.props.TrackersTableElementBody.name}</td>
                <td>{this.props.TrackersTableElementBody.driverBalance}</td>
                <td>+7{this.props.TrackersTableElementBody.driverPhone}</td>
                <td>
                    <button
                        type="button"
                        className="btn btn-secondary btn-xs"
                        data-toggle="modal"
                        data-target="#trackerInfoModal"
                        onClick={this.handleClick}>
                        Информация
                    </button>
                    <span className="modal fade" id="trackerInfoModal"
                        role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <TrackerInfoModal />
                    </span>
                </td>
            </tr>
        );
    }
}