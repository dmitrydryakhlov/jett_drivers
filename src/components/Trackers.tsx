import React from 'react';
import { TrackersTable } from '../components/TrackersTable';
import { connect } from 'react-redux';
import {
    createTrackersType,
    changeTrackerType,
    getTrackersTypes,
    getTrackersTypeDetail
} from '../actions/trackersTypeActions'
import {
    createTracker,
    getTrackers,
    getTrackerDetail
} from '../actions/trackersAction'
import { TrackersTypeTable } from './TrackersTypeTable';
import { iGPSTypeTracker, iGPSSmallTracker, iGPSTracker, iDriver, iPagintion } from '../models/models';
import Pagination from './Pagination';

type TrackersProps = {
    trackersTypeList: iGPSTypeTracker[],
    createTrackersType: Function,
    changeTrackerType: Function,
    getTrackersTypes: Function,
    getTrackersTypeDetail: Function,

    trackersList: iGPSSmallTracker[],
    createTracker: Function,
    changeTracker: Function,
    getTrackers: Function,
    getTrackerDetail: Function,

    driversList: iDriver[],
    pagination: iPagintion,
    changePage: Function
}

class Trackers extends React.Component<TrackersProps> {

    state = {
        newTypeName: undefined,
        newTypeBlockSMS: undefined,
        newTypeUnblockSMS: undefined,

        name: undefined,
        phone: 0,
        recurringPayment: undefined,
        type: undefined,
        driver: undefined,
        trackerPenalty: 0
    }

    onNewTypeNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ ...this.state, newTypeName: event.currentTarget.value });
    }
    onNewTypeBlockSMSChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ ...this.state, newTypeBlockSMS: event.currentTarget.value });
    }
    onNewTypeUnblockSMSChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ ...this.state, newTypeUnblockSMS: event.currentTarget.value });
    }

    onNewTrackerNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ ...this.state, name: event.currentTarget.value });
    }
    onNewTrackerPhoneChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            ...this.state,
            phone: event.currentTarget.value,
            driver: event.currentTarget.value
        });
    }
    onNewTrackerPaymentChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ ...this.state, recurringPayment: event.currentTarget.value });
    }
    onNewTrackerTypeChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        this.setState({ ...this.state, type: event.currentTarget.value });
    }
    onNewTrackerDriverChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        this.setState({ ...this.state, driver: event.currentTarget.value });
    }
    onNewTrackerPenaltyChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ ...this.state, trackerPenalty: event.currentTarget.value });
    }

    createNewTypeHandler = (event: React.MouseEvent<HTMLElement>) => {
        this.props.createTrackersType(
            this.state.newTypeName,
            this.state.newTypeBlockSMS,
            this.state.newTypeUnblockSMS,
        );
    }

    createNewTrackerHandler = (event: React.MouseEvent<HTMLElement>) => {
        this.props.createTracker({
            name: this.state.name,
            phone: this.state.phone,
            recurringPayment: this.state.recurringPayment,
            type: this.state.type,
            driver: this.state.driver,
            trackerPenalty: this.state.trackerPenalty
        });
    }

    validateType() {
        return (
            this.state.newTypeName &&
            this.state.newTypeBlockSMS &&
            this.state.newTypeUnblockSMS
        )
    }

    validateTracker() {
        return (
            this.state.driver &&
            this.state.name &&
            this.state.phone &&
            this.state.type &&
            this.state.trackerPenalty &&
            this.state.recurringPayment
        )
    }

    render() {
        return (
            <div className="TrackerPage">
                <div className="btn-group float-right" role="group">
                    <button type="button" className="btn btn-success" data-toggle="modal" data-target="#addTrackerTypeModal">
                        Добавить тип
                    </button>
                    <button type="button" className="btn btn-success" data-toggle="modal" data-target="#addTrackerModal">
                        Добавить трекер
                    </button>
                </div>
                <div className="modal fade" id="addTrackerTypeModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="addTrackerTypeModal">Тип GPS трекера</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label htmlFor="newTrackerName">Название типа</label>
                                        <input type="text" className="form-control" id="newTrackerName"
                                            onChange={this.onNewTypeNameChange} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="newTrackerPhone">СМС при блокировке</label>
                                        <input type="text" className="form-control" id="newTrackerPhone"
                                            onChange={this.onNewTypeBlockSMSChange} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="newTrackerDriver">СМС при разблокировке</label>
                                        <input type="text" className="form-control" id="newTrackerDriver"
                                            onChange={this.onNewTypeUnblockSMSChange} />
                                    </div>
                                </form>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                                    <button type="button" className="btn btn-primary"
                                        onClick={this.createNewTypeHandler}>Сохранить</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="addTrackerModal" role="dialog" aria-labelledby="addTrackerModal" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="addTrackerModal">Новый трекер</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label htmlFor="newTrackerName">Название</label>
                                        <input type="text" className="form-control" id="newTrackerName"
                                            onChange={this.onNewTrackerNameChange} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="newTrackerGroup">Телефон водителя</label>
                                        <div className="input-group-prepend" id="newTrackerGroup">
                                            <span className="input-group-text" id="basic-addon1">+7</span>
                                            <input type="text" className="form-control" id="newTrackerPhone"
                                                placeholder="номер" aria-label="Username"
                                                aria-describedby="basic-addon1"
                                                onChange={this.onNewTrackerPhoneChange} />
                                        </div>

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="newTrackerType">Тип трекера</label>
                                        <select id="newTrackerType" className="form-control"
                                            onChange={this.onNewTrackerTypeChange}>
                                            <option>выберете тип</option>
                                            {this.props.trackersTypeList.map((item, index) => {
                                                return <option value={item.name} key={index}>{item.name}</option>
                                            })}
                                        </select>
                                    </div>
                                    {/* <div className="form-group">
                                        <label htmlFor="newTrackerDriver">Водитель</label>
                                        <select id="newTrackerDriver" className="form-control"
                                            onChange={this.onNewTrackerDriverChange}>
                                            <option>выберете водителя</option>
                                            {this.props.trackersTypeList.map((item, index) => {
                                                return <option value={item.name} key={index}>{item.name}</option>
                                            })}
                                        </select>
                                    </div> */}
                                    <div className="form-group">
                                        <label htmlFor="newTrackerRecurringPayment">Регулярно списываемая сумма</label>
                                        <input type="text" className="form-control" id="newTrackerRecurringPayment"
                                            placeholder="ТГ" onChange={this.onNewTrackerPaymentChange} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="newTrackerPenalty">Штраф</label>
                                        <input type="text" className="form-control" id="newTrackerPenalty"
                                            onChange={this.onNewTrackerPenaltyChange} />
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                                        <button type="button" className="btn btn-primary"
                                            onClick={this.createNewTrackerHandler}>Сохранить</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <TrackersTypeTable
                    trackersTypeList={this.props.trackersTypeList}
                    changeTrackerType={this.props.changeTrackerType}
                    getTrackersTypes={this.props.getTrackersTypes}
                    getTrackersTypeDetail={this.props.getTrackersTypeDetail} />
                <TrackersTable
                    trackersList={this.props.trackersList}
                    changeTracker={this.props.changeTracker}
                    getTrackers={this.props.getTrackers}
                    getTrackerDetail={this.props.getTrackerDetail} />
                <Pagination Pagination={this.props.pagination} changePage={this.props.changePage} id={0} />
            </div>
        )
    }
}

const mapStateToProps = (store: any) => {
    return {
        trackersTypeList: store.trackersType.trackersTypeList,
        trackersList: store.trackers.trackersList,
        driversList: store.drivers.driversList,
        pagination: store.pagination.trackersPagination
    }
}

const mapDispatchToProps = (dispatch: any) => ({
    createTrackersType: (
        name: number,
        recurringPaymentSmsBlocked: string,
        recurringPaymentSmsUnblocked: string
    ) => dispatch(createTrackersType(name, recurringPaymentSmsBlocked, recurringPaymentSmsUnblocked)),
    changeTrackerType: (
        id: string,
        name: string,
        recurringPaymentSmsBlocked: string,
        recurringPaymentSmsUnblocked: string
    ) => dispatch(changeTrackerType(id, name, recurringPaymentSmsBlocked, recurringPaymentSmsUnblocked)),
    getTrackersTypes: () => dispatch(getTrackersTypes()),
    getTrackersTypeDetail: (id: string) => dispatch(getTrackersTypeDetail(id)),

    changeTracker: (
        id: string,
        name: string,
        recurringPaymentSmsBlocked: string,
        recurringPaymentSmsUnblocked: string
    ) => dispatch(changeTrackerType(id, name, recurringPaymentSmsBlocked, recurringPaymentSmsUnblocked)),
    createTracker: (tracker: iGPSTracker) => dispatch(createTracker(tracker)),
    getTrackers: () => dispatch(getTrackers()),
    getTrackerDetail: (id: string) => dispatch(getTrackerDetail(id)),
    changePage: (page: number, limit: number) => dispatch(getTrackers(page, limit))
})

export default connect(mapStateToProps, mapDispatchToProps)(Trackers);