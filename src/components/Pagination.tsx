import React from 'react';
import { TrackersTable } from '../components/TrackersTable';
import { connect } from 'react-redux';
import { iPagintion } from '../models/models';

type paginationProps = {
    Pagination: iPagintion,
    changePage: Function,
    id: number
}
class Pagination extends React.Component<paginationProps> {

    constructor(props: paginationProps) {
        super(props);
        this.decrementPage = this.decrementPage.bind(this);
        this.incrementPage = this.incrementPage.bind(this);
    }

    decrementPage() {
        this.props.changePage(this.props.Pagination.page - 1, this.props.Pagination.limit, this.props.id);
    }
    incrementPage() {
        this.props.changePage(this.props.Pagination.page + 1, this.props.Pagination.limit, this.props.id);
    }

    render() {
        return (
            <nav aria-label="...">
                <ul className="pagination justify-content-center">
                    <li className="page-item">
                        <a className="page-link" onClick={this.decrementPage}>Назад</a>
                    </li>
                    <li className="page-item active" aria-current="page">
                        <span className="page-link">
                            {this.props.Pagination.page}
                        </span>
                    </li>
                    <li className="page-item">
                        <a className="page-link" onClick={this.incrementPage}>След.</a>
                    </li>
                </ul>
            </nav>
        )
    }
}

export default connect()(Pagination);