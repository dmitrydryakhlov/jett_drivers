import React from 'react';
import { TrackerTypeTableElement } from './TrackerTypeTableElement';
import { iGPSTypeTracker, iGPSSmallTypeTracker } from '../models/models';

type trackersTableProps = {
    trackersTypeList: iGPSSmallTypeTracker[];
    //trackersTypeDetail: iGPSTypeSmallTracker;
    changeTrackerType: Function
    getTrackersTypes: Function
    getTrackersTypeDetail: Function
};
export class TrackersTypeTable extends React.Component<trackersTableProps> {

    componentDidMount() {
        this.props.getTrackersTypes();
    }

    renderRow(items: iGPSSmallTypeTracker[]) {
        return items.map((item, index) =>
            <TrackerTypeTableElement
                TrackerTypeTableElementBody={item}
                changeTrackerType={this.props.changeTrackerType}
                getTrackersTypeDetail={this.props.getTrackersTypeDetail}
                key={index} />
        )
    }

    render() {
        return (
            <table className="table table-bordered ">
                <thead>
                    <tr>
                        <th scope="col align-center">Название</th>
                        <th scope="col align-center">Информация</th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderRow(this.props.trackersTypeList)}
                </tbody>
            </table>
        );
    }
}
