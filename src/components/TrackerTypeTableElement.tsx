import React from 'react';
import { iGPSSmallTypeTracker } from '../models/models';
import TrackerTypeInfoModal from './TrackerTypeInfoModal';

type TrackerTypeTableElementProps = {
    TrackerTypeTableElementBody: iGPSSmallTypeTracker,
    changeTrackerType: Function,
    getTrackersTypeDetail: Function
}

export class TrackerTypeTableElement extends React.Component<TrackerTypeTableElementProps> {

    constructor(props: TrackerTypeTableElementProps) {
        super(props)
        this.btnHandler = this.btnHandler.bind(this);
    }

    btnHandler() {
        this.props.getTrackersTypeDetail(this.props.TrackerTypeTableElementBody.id);
    }
    render() {
        return (
            <tr>
                <td>{this.props.TrackerTypeTableElementBody.name}</td>
                <td>
                    <button
                        type="button"
                        className="btn btn-secondary"
                        data-toggle="modal"
                        data-target="#changeTrackerTypeModal"
                        onClick={this.btnHandler}>
                        Изменить
                    </button>
                    <div className="modal fade" id="changeTrackerTypeModal"
                        role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <TrackerTypeInfoModal />
                    </div>
                </td>
            </tr >
        );
    }
}