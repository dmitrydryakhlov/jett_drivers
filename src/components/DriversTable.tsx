import React from 'react';
import DriversTableElement from './DriversTableElement';
import { iSmallDriver } from '../models/models';

type driversTableProps = {
    driversList: iSmallDriver[];
};
export class DriversTable extends React.Component<driversTableProps> {


    renderRow(items: iSmallDriver[]) {
        let rows = items.map((item, index) => {
            return (
                <DriversTableElement iSmallDriver={item} key={index} />
            )
        })
        return rows;
    }

    render() {
        const { driversList } = this.props;
        return (
            <table className="table table-striped ">
                <thead>
                    <tr>
                        <th scope="col">Имя</th>
                        <th scope="col">Город</th>
                        <th scope="col">Баланс</th>
                        <th scope="col">Телефон</th>
                        <th scope="col">Информация</th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderRow(driversList)}
                </tbody>
            </table>
        );
    }
}
