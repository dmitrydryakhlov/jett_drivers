import React from 'react';
import { connect } from 'react-redux';
import { changeTrackersModal } from '../actions/modalsActions';
import { iGPSTracker, iTrackerModal, iGPSTypeTracker } from '../models/models';
import { changeTracker } from '../actions/trackersAction';


interface iTTModalProps {
    trackersDetail: iGPSTracker
    trackersModal: iTrackerModal
    changeTracker: Function
    changeTrackersModal: Function
    trackersTypeList: iGPSTypeTracker[]
}

class TrackerInfoModal extends React.Component<iTTModalProps> {

    onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { id, value } = event.currentTarget
        this.props.changeTrackersModal({ ...this.props.trackersModal, [id]: value });
    }

    onSelectChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const { id, value } = event.currentTarget
        this.props.changeTrackersModal({ ...this.props.trackersModal, [id]: value });
    }

    changeTracker = (event: React.MouseEvent<HTMLElement>) => {
        this.props.changeTracker(this.props.trackersDetail.id, this.props.trackersModal);
    }

    blockTracker = () => this.props.changeTrackersModal({
        ...this.props.trackersModal,
        blocked: !this.props.trackersModal.blocked
    });

    render() {
        return (
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">Информация о трекере</h5>
                    </div>
                    <div className="modal-body">
                        <form>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="newTrackerName">Название</label>
                                    <input type="text" className="form-control" id="name"
                                        value={this.props.trackersModal.name}
                                        onChange={this.onInputChange} />
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="newTrackerPhone">Телефон</label>
                                    <input type="text" className="form-control" id="phone"
                                        value={this.props.trackersModal.phone}
                                        onChange={this.onInputChange} />
                                </div>
                            </div>
                            <div className="form-group">
                                <label htmlFor="newTrackerType">Тип трекера</label>
                                <select id="type" className="form-control"
                                    onChange={this.onSelectChange}>
                                    <option>{this.props.trackersModal.type}</option>
                                    {this.props.trackersTypeList.map((item, index) => {
                                        return <option value={item.id} key={index}>{item.name}</option>
                                    })}
                                </select>
                            </div>
                            <div className="form-group">
                                <label htmlFor="newTrackerDriver">Водитель</label>
                                {/* <select id="newTrackerDriver" className="form-control"
                                    onChange={this.onSelectChange}>
                                    <option>{this.props.trackersModal.driver}</option>
                                    {this.props.trackersList.map((item, index) => {
                                        return <option value={item.id}>{item.name}</option>
                                    })}
                                </select> */}
                            </div>
                            <div className="form-group">
                                <label htmlFor="newTrackerRecurringPayment">Регулярно списываемая сумма</label>
                                <input type="text" className="form-control" id="recurringPayment"
                                    placeholder="ТГ" onChange={this.onInputChange}
                                    value={this.props.trackersModal.recurringPayment} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="newTrackerPenalty">Штраф</label>
                                <input type="text" className="form-control" id="trackerPenalty"
                                    onChange={this.onInputChange}
                                    value={this.props.trackersModal.trackerPenalty} />
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-warning" onClick={this.blockTracker}>
                                    {this.props.trackersModal.blocked ? 'Разблокировать' : 'Заблокировать'}</button>
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                                <button type="button" className="btn btn-primary" onClick={this.changeTracker}>Сохранить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (store: any) => {
    return {
        trackersDetail: store.trackers.trackersDetail,
        trackersModal: store.modals.trackersModal,
        trackersTypeList: store.trackersType.trackersTypeList,
    }
}

const mapDispatchToProps = (dispatch: any) => ({
    changeTrackersModal: (modalsData: iTrackerModal) => dispatch(changeTrackersModal(modalsData)),
    changeTracker: (id: string, modalsData: iTrackerModal) => dispatch(changeTracker(id, modalsData))
})

export default connect(mapStateToProps, mapDispatchToProps)(TrackerInfoModal);