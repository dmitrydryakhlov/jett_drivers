/** 
 * GPS Tracker
 * 
 *  @param {string} id 
    @param {string} name
    @param {number} phone
    @param {boolean} blocked
    @param {number} recurringPayment 
    @param {number} trackerPenalty
    @param {string} type
    @param {string} driver
 */
export interface iGPSTracker {
    id: string
    name: string
    phone: number
    blocked: boolean
    recurringPayment: number
    trackerPenalty: number
    type: string
    driver: string
}
/** 
 * GPS Small Tracker
 * 
 *  @param {string} id 
 *  @param {string} name
 *  @param {number} driverBalance
 *  @param {number} driverPhone
 */
export interface iGPSSmallTracker {
    id: string
    name: string
    driverBalance: number
    driverPhone: number
}

/**
 * car
 * 
 * @param {string} model
 * @param {string} mark
 * @param {string} color
 * @param {string} number
 */
export interface iCar {
    model: string;
    mark: string;
    color: string;
    number: string;
}

/** 
 * driverChange
 * 
 *  @param {string} name
 *  @param {number} phone
 *  @param {string} city
 *  @param {iCar} car
 */
export interface iDriverChange {
    name: string;
    phone: string;
    city: number;
    car: iCar;
}


/**
 * New driver
 * 
 * @param {string} name
 * @param {number} phone
 * @param {string} city
 * @param {string} promocode
 * @param {iCar} car 
 */
export interface iNewDriver {
    name: string;
    phone: number;
    city: string;
    promocode: string;
    car: iCar;
}

/**
 * Driver
 * 
 * @param {number} id
 * @param {string} name
 * @param {number} phone
 * @param {number} balance
 * @param {string} city
 * @param {iCar} car 
 */
export interface iDriver {
    id: number;
    name: string;
    phone: number;
    balance: number;
    city: string;
    car: iCar;
}

/**
 * Driver Small
 * 
 * @param {number} id
 * @param {string} name
 * @param {string} city
 * @param {number} balance
 * @param {number} phone 
 */
export interface iSmallDriver {
    id: number;
    name: string;
    city: string;
    balance: number;
    phone: number;
}

/**
 * Pagination
 * 
 * @param {number} titalPages число доступных страниц
 * @param {number} page номер текущей страницы
 * @param {number} total общее число записей
 * @param {number} limit Число записей на странице
 * @param {number} prev Номер предыдущей страницы
 * @param {number} next Номер следущей страницы
 */
export interface iPagintion {
    titalPages: number;
    page: number;
    total: number;
    limit: number;
    prev: number;
    next: number;
}

/**
 * @param {string} id
 * @param {string} name
 * @param {string} recurringPaymentSmsBlocked
 * @param {string} recurringPaymentSmsUnblocked
 */
export interface iGPSTypeTracker {
    id: string
    name: string
    recurringPaymentSmsBlocked: string
    recurringPaymentSmsUnblocked: string
}

/**
 * @param {string} name
 * @param {string} recurringPaymentSmsBlocked
 * @param {string} recurringPaymentSmsUnblocked
 */
export interface iTrackerTypeModal {
    name: string
    recurringPaymentSmsBlocked: string
    recurringPaymentSmsUnblocked: string

}
/**
 * @param {string} name
 * @param {string} phone
 * @param {boolean} blocked
 * @param {number} recurringPayment
 * @param {number} trackerPenalty
 * @param {string} type
 * @param {string} driver
 */
export interface iTrackerModal {
    name: string,
    phone: string,
    blocked: true,
    recurringPayment: number,
    trackerPenalty: number,
    type: string,
    driver: string
}


/**
 * @param {string} id
 * @param {string} name
 */
export interface iGPSSmallTypeTracker {
    id: string
    name: string
}

/**
 * @param {number} amount
 * @param {string} id
 * @param {string} comment
 */
export interface iHistory {
    id: number
    amount: string
    comment: string
}



