import {
    GET_TRACKERS_REQUEST,
    GET_TRACKERS_SUCCESS,
    GET_TRACKERS_DETAIL_REQUEST,
    GET_TRACKERS_DETAIL_SUCCESS,
    POST_TRACKERS_CREATE,
    POST_TRACKERS_CREATE_SUCCESS,
    PUT_TRACKERS_CHANGE,
    PUT_TRACKERS_CHANGE_SUCCESS
} from "../actions/trackersAction";


const initialState = {
    trackersList: [],
    trackersDetail: {},
    isFetching: false
}

export function trackersReducer(state = initialState, action) {
    switch (action.type) {
        case GET_TRACKERS_REQUEST: {
            return {
                ...state,
                isFetching: true
            };
        }
        case GET_TRACKERS_SUCCESS: {
            return {
                ...state,
                trackersList: action.payload.trackersList,
                isFetching: false
            };
        }

        case GET_TRACKERS_DETAIL_REQUEST: {
            return { ...state, }
        }
        case GET_TRACKERS_DETAIL_SUCCESS: {
            return { ...state, trackersDetail: action.payload }
        }

        case POST_TRACKERS_CREATE: {
            return { ...state }
        }
        case POST_TRACKERS_CREATE_SUCCESS: {
            return { ...state }
        }

        case PUT_TRACKERS_CHANGE: {
            return { ...state }
        }
        case PUT_TRACKERS_CHANGE_SUCCESS: {
            return { ...state }
        }

        default: {
            return state;
        }
    }
}
