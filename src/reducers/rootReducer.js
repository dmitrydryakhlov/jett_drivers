import { combineReducers } from 'redux';
import { driverReducer } from './drivers';
import { trackersReducer } from './trackers';
import { trackersTypeReducer } from './trackersType'
import { modalsReducer } from './modals';
import { paginationReducer } from './pagination';

export const rootReducer = combineReducers({
    drivers: driverReducer,
    trackers: trackersReducer,
    modals: modalsReducer,
    trackersType: trackersTypeReducer,
    pagination: paginationReducer
})