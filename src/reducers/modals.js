import {
    TRACKERS_TYPE_MODALS_CHANGE,
    TRACKERS_MODALS_CHANGE,
    DRIVERS_MODALS_CHANGE
} from "../actions/modalsActions";
import { GET_TRACKERS_TYPE_DETAIL_SUCCESS } from "../actions/trackersTypeActions";
import { GET_TRACKERS_DETAIL_SUCCESS } from "../actions/trackersAction";
import { GET_DRIVERS_DETAIL_SUCCESS } from "../actions/driversActions";


const initialState = {
    trackersTypeModal: {},
    trackersModal: {},
    driversModal: {}
}

export function modalsReducer(state = initialState, action) {
    switch (action.type) {

        case TRACKERS_TYPE_MODALS_CHANGE: {
            return { ...state, trackersTypeModal: action.payload };
        }
        case GET_TRACKERS_TYPE_DETAIL_SUCCESS: {
            return { ...state, trackersTypeModal: action.payload }
        }

        case TRACKERS_MODALS_CHANGE: {
            return { ...state, trackersModal: action.payload };
        }
        case GET_TRACKERS_DETAIL_SUCCESS: {
            return { ...state, trackersModal: action.payload }
        }

        case DRIVERS_MODALS_CHANGE: {
            return { ...state, driversModal: action.payload };
        }
        case GET_DRIVERS_DETAIL_SUCCESS: {
            return { ...state, driversModal: action.payload }
        }

        default: {
            return state;
        }

    }
}
