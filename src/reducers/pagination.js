import {
    TRACKERS_PAGINATION_CHANGE,
    DRIVERS_PAGINATION_CHANGE,
    HISTORY_PAGINATION_CHANGE
} from "../actions/paginationAction";


const initialState = {
    trackersPagination: {},
    driversPagination: {},
    historyPagination: {}
}

export function paginationReducer(state = initialState, action) {
    switch (action.type) {
        case TRACKERS_PAGINATION_CHANGE: {
            return { ...state, trackersPagination: action.payload };
        }
        case DRIVERS_PAGINATION_CHANGE: {
            return { ...state, driversPagination: action.payload };
        }
        case HISTORY_PAGINATION_CHANGE: {
            return { ...state, historyPagination: action.payload };
        }

        default: {
            return state;
        }

    }
}
