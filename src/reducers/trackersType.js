import {
    GET_TRACKERS_TYPE_REQUEST,
    GET_TRACKERS_TYPE_SUCCESS,
    GET_TRACKERS_TYPE_DETAIL_REQUEST,
    GET_TRACKERS_TYPE_DETAIL_SUCCESS,
    POST_TRACKERS_TYPE_CREATE,
    POST_TRACKERS_TYPE_CREATE_SUCCESS,
    PUT_TRACKERS_TYPE_CHANGE,
    PUT_TRACKERS_TYPE_CHANGE_SUCCESS
} from "../actions/trackersTypeActions";


const initialState = {
    trackersTypeList: [],
    trackersTypeDetail: {},
    isFetching: false
}

export function trackersTypeReducer(state = initialState, action) {
    switch (action.type) {
        case GET_TRACKERS_TYPE_REQUEST: {
            return {
                ...state,
                isFetching: true
            };
        }
        case GET_TRACKERS_TYPE_SUCCESS: {
            return {
                ...state,
                trackersTypeList: action.payload,
                isFetching: false
            };
        }

        case GET_TRACKERS_TYPE_DETAIL_REQUEST: {
            return { ...state, }
        }
        case GET_TRACKERS_TYPE_DETAIL_SUCCESS: {
            return { ...state, trackersTypeDetail: action.payload }
        }

        case POST_TRACKERS_TYPE_CREATE: {
            return { ...state }
        }
        case POST_TRACKERS_TYPE_CREATE_SUCCESS: {
            return { ...state }
        }

        case PUT_TRACKERS_TYPE_CHANGE: {
            return { ...state }
        }
        case PUT_TRACKERS_TYPE_CHANGE_SUCCESS: {
            return { ...state }
        }

        default: {
            return state;
        }

    }
}
