import {
    GET_DRIVERS_REQUEST,
    GET_DRIVERS_SUCCESS,
    GET_DRIVERS_DETAIL_REQUEST,
    GET_DRIVERS_DETAIL_SUCCESS,
    POST_DRIVERS_CREATE,
    POST_DRIVERS_CREATE_SUCCESS,
    GET_DRIVERS_HISTORY,
    GET_DRIVERS_HISTORY_SUCCESS,

} from "../actions/driversActions";


const initialState = {
    driversList: [],
    driversDetail: {},
    isFetching: false,
    balanceHistories: []
}

export function driverReducer(state = initialState, action) {
    switch (action.type) {
        case GET_DRIVERS_REQUEST: {
            return {
                ...state,
                driversList: action.payload,
                isFetching: true
            };
        }
        case GET_DRIVERS_SUCCESS: {
            return {
                ...state,
                driversList: action.payload.driversList,
                isFetching: false
            };
        }

        case GET_DRIVERS_DETAIL_REQUEST: {
            return { ...state, driversDetail: action.payload }
        }
        case GET_DRIVERS_DETAIL_SUCCESS: {
            return { ...state, driversDetail: action.payload }
        }

        case GET_DRIVERS_HISTORY: {
            return { ...state, balanceHistories: action.payload }
        }
        case GET_DRIVERS_HISTORY_SUCCESS: {
            return { ...state, balanceHistories: action.payload }
        }

        case POST_DRIVERS_CREATE: {
            return { ...state }
        }
        case POST_DRIVERS_CREATE_SUCCESS: {
            return { ...state }
        }


        default: {
            return state;
        }

    }
}
