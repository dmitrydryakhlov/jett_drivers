import React, { Component } from 'react';
import Trackers from './components/Trackers';
import Drivers from './components/Drivers';

export class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="container">
          <div className="row">
            <div className="col-2">
              <div className="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <a className="nav-link active" id="v-pills-home-tab" data-toggle="pill"
                  href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Водители</a>
                <a className="nav-link" id="v-pills-profile-tab" data-toggle="pill"
                  href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">GPS-Трекеры</a>
              </div>
            </div>
            <div className="col-10">
              <div className="tab-content" id="v-pills-tabContent">
                <div className="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                  <Drivers />
                </div>
                <div className="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                  <Trackers />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

