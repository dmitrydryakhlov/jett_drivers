export const TRACKERS_TYPE_MODALS_CHANGE = 'TRACKERS_TYPE_MODALS_CHANGE';
export const TRACKERS_MODALS_CHANGE = 'TRACKERS_MODALS_CHANGE';
export const DRIVERS_MODALS_CHANGE = 'DRIVERS_MODALS_CHANGE';


export function changeTrackersTypeModal(modalData) {
    return dispatch => {
        dispatch({
            type: TRACKERS_TYPE_MODALS_CHANGE,
            payload: modalData
        });
    }
}

export function changeTrackersModal(modalData) {
    return dispatch => {
        dispatch({
            type: TRACKERS_MODALS_CHANGE,
            payload: modalData
        });
    }
}

export function changeDriversModal(modalData) {
    return dispatch => {
        dispatch({
            type: DRIVERS_MODALS_CHANGE,
            payload: modalData
        });
    }
}