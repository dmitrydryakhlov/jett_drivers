import { TRACKERS_PAGINATION_CHANGE } from "./paginationAction";

export const GET_TRACKERS_REQUEST = 'GET_TRACKERS_REQUEST';
export const GET_TRACKERS_SUCCESS = 'GET_TRACKERS_SUCCESS';
export const GET_TRACKERS_DETAIL_REQUEST = 'GET_TRACKERS_DETAIL_REQUEST';
export const GET_TRACKERS_DETAIL_SUCCESS = 'GET_TRACKERS_DETAIL_SUCCESS';
export const POST_TRACKERS_CREATE = 'POST_TRACKERS_CREATE';
export const POST_TRACKERS_CREATE_SUCCESS = 'POST_TRACKERS_CREATE_SUCCESS';
export const PUT_TRACKERS_CHANGE = 'PUT_TRACKERS_CHANGE';
export const PUT_TRACKERS_CHANGE_SUCCESS = 'PUT_TRACKERS_CHANGE_SUCCESS';

export function getTrackers(page = 0, limit = 0) {

    const url = `https://zaqwe.info/api/gps_trackers?page=${page}&limit=${limit}`;
    const ecodedAdmin = window.btoa('admin:qwerty');
    return dispatch => {
        dispatch({ type: GET_TRACKERS_REQUEST });
        fetch(`${url}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': `Basic ${ecodedAdmin}`,
            },
            responseType: 'json',
        }).then((res) => {
            if (res.status === 500) {
                alert('ошибка сервера');
            }
            return res.json()
        })
            .then(response => {
                const { gpsTrackers, pagination } = response;
                dispatch({
                    type: GET_TRACKERS_SUCCESS,
                    payload: {
                        trackersList: gpsTrackers,
                    }
                })
                dispatch({
                    type: TRACKERS_PAGINATION_CHANGE,
                    payload: pagination
                })
            })
    }
}

export function getTrackerDetail(id) {

    const url = `https://zaqwe.info/api/gps_trackers/${id}`;
    const ecodedAdmin = window.btoa('admin:qwerty');
    return dispatch => {
        dispatch({ type: GET_TRACKERS_DETAIL_REQUEST });
        fetch(`${url}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': `Basic ${ecodedAdmin}`,
            },
            responseType: 'json',
        }).then((res) => {
            if (res.status === 404) {
                alert('Трекера с таким id не существует');
            }
            if (res.status === 500) {
                alert('Ошибка сервера');
            }
            return res.json();
        }).then(response => {
            if (response) {
                dispatch({
                    type: GET_TRACKERS_DETAIL_SUCCESS,
                    payload: response,
                })
            }
        })
    }
}


export function createTracker(tracker) {

    const url = `https://zaqwe.info/api/gps_trackers`;
    const ecodedAdmin = window.btoa('admin:qwerty');

    return async dispatch => {
        dispatch({ type: POST_TRACKERS_CREATE });
        fetch(`${url}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': `Basic ${ecodedAdmin}`,
            },
            body: JSON.stringify(tracker),
            responseType: 'json',
        }).then((res) => {
            if (res.status === 200) {
                alert('трекер успешно создан');
            }
            if (res.status === 400) {
                alert('Заполните все обязательные поля');
            }
            if (res.status === 402) {
                alert('Недостаточно средств для ежедневной оплаты');
            }
            if (res.status === 404) {
                alert('Водителя/трекера с таким id не существует');
            }
            if (res.status === 500) {
                alert('ошибка сервера');
            }
            return res.json()
        }).then(response => {
            dispatch({
                type: POST_TRACKERS_CREATE_SUCCESS,
                payload: {
                    id: response.id,
                }
            })
        })
    }
}

export function changeTracker(id, tracker) {

    const url = `https://zaqwe.info/api/gps_trackers/${id}`;
    const ecodedAdmin = window.btoa('admin:qwerty');

    return dispatch => {

        dispatch({ type: PUT_TRACKERS_CHANGE });
        fetch(`${url}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': `Basic ${ecodedAdmin}`,
            },
            body: JSON.stringify(tracker),
            responseType: 'json',
        }).then((res) => {
            if (res.status === 200) {
                alert('Трекер изменён');
            }
            if (res.status === 404) {
                alert('Трекера с таким id не существует');
            }
            if (res.status === 500) {
                alert('Ошибка сервера');
            }
            return res.json();
        }).then(response => {
            dispatch({ type: PUT_TRACKERS_CHANGE_SUCCESS })
        })

    }
}