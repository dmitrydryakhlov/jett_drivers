export const GET_TRACKERS_TYPE_REQUEST = 'GET_TRACKERS_TYPE_REQUEST';
export const GET_TRACKERS_TYPE_SUCCESS = 'GET_TRACKERS_TYPE_SUCCESS';
export const GET_TRACKERS_TYPE_DETAIL_REQUEST = 'GET_TRACKERS_TYPE_DETAIL_REQUEST';
export const GET_TRACKERS_TYPE_DETAIL_SUCCESS = 'GET_TRACKERS_TYPE_DETAIL_SUCCESS';
export const POST_TRACKERS_TYPE_CREATE = 'POST_TRACKERS_TYPE_CREATE';
export const POST_TRACKERS_TYPE_CREATE_SUCCESS = 'POST_TRACKERS_TYPE_CREATE_SUCCESS';
export const PUT_TRACKERS_TYPE_CHANGE = 'PUT_TRACKERS_TYPE_CHANGE';
export const PUT_TRACKERS_TYPE_CHANGE_SUCCESS = 'PUT_TRACKERS_TYPE_CHANGE_SUCCESS';

export function getTrackersTypes() {

    const url = `https://zaqwe.info/api/gps_tracker_types`;
    const ecodedAdmin = window.btoa('admin:qwerty');
    return async dispatch => {
        dispatch({ type: GET_TRACKERS_TYPE_REQUEST });
        try {
            const response = await fetch(`${url}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Authorization': `Basic ${ecodedAdmin}`,
                },
                responseType: 'json',
            }).then((res) => res.json());
            if (response) {
                dispatch({
                    type: GET_TRACKERS_TYPE_SUCCESS,
                    payload: response
                })
            }
        }
        catch (err) {
            console.error('Error', err);
        }
    }
}

export function getTrackersTypeDetail(id) {

    const url = `https://zaqwe.info/api/gps_tracker_types/${id}`;
    const ecodedAdmin = window.btoa('admin:qwerty');
    return dispatch => {
        dispatch({ type: GET_TRACKERS_TYPE_DETAIL_REQUEST });
        fetch(`${url}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': `Basic ${ecodedAdmin}`,
            },
            responseType: 'json',
        }).then((res) => {
            if (res.status === 400) {
                alert('Тип трекера с таким id не существует')
            }
            if (res.status === 500) {
                alert('Ошибка сервера')
            }
            return res.json();
        }).then(response => {
            dispatch({
                type: GET_TRACKERS_TYPE_DETAIL_SUCCESS,
                payload: response,
            })
        })
    }
}

export function createTrackersType(name, recurringPaymentSmsBlocked, recurringPaymentSmsUnblocked) {

    const url = `https://zaqwe.info/api/gps_tracker_types`;
    const ecodedAdmin = window.btoa('admin:qwerty');

    return dispatch => {
        dispatch({ type: POST_TRACKERS_TYPE_CREATE });
        fetch(`${url}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': `Basic ${ecodedAdmin}`,
            },
            body: JSON.stringify({ name, recurringPaymentSmsBlocked, recurringPaymentSmsUnblocked }),
            responseType: 'json',
        }).then(res => {
            if (res.status === 200) {
                alert('Новый тип успешно создан');
            }
            if (res.status === 400) {
                alert('заполните все поля');
            }
            if (res.status === 500) {
                alert('Ошибка сервера');
            }
            return res.json();
        }).then(response => {
            dispatch({
                type: POST_TRACKERS_TYPE_CREATE_SUCCESS,
                payload: {
                    id: response.id,
                }
            })
        })
    }
}

export function changeTrackerType(id, name, recurringPaymentSmsBlocked, recurringPaymentSmsUnblocked) {

    const url = `https://zaqwe.info/api/gps_tracker_types/${id}`;
    const ecodedAdmin = window.btoa('admin:qwerty');

    return dispatch => {

        dispatch({ type: PUT_TRACKERS_TYPE_CHANGE });
        fetch(`${url}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': `Basic ${ecodedAdmin}`,
            },
            body: JSON.stringify({ name, recurringPaymentSmsBlocked, recurringPaymentSmsUnblocked }),
            responseType: 'json',
        }).then(res => {
            if (res.status === 200) {
                alert('Тип изменён');
            }
            if (res.status === 200) {
                alert('Тип трекера с таким id не существует');
            }
            if (res.status === 200) {
                alert('Ошибка сервера');
            }
            return res.json()
        }).then(response => {
            dispatch({ type: PUT_TRACKERS_TYPE_CHANGE_SUCCESS })
        })
    }
}