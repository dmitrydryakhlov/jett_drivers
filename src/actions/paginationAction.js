export const TRACKERS_PAGINATION_CHANGE = 'TRACKERS_PAGINATION_CHANGE';
export const DRIVERS_PAGINATION_CHANGE = 'DRIVERS_PAGINATION_CHANGE';
export const HISTORY_PAGINATION_CHANGE = 'HISTORY_PAGINATION_CHANGE';

export function driversChangePage(pagination) {
    return async dispatch => {
        dispatch({ type: DRIVERS_PAGINATION_CHANGE, payload: pagination });
    }
}
export function trackersChangePage(pagination) {
    return dispatch => {
        dispatch({ type: TRACKERS_PAGINATION_CHANGE, payload: pagination });
    }
}
export function historyChangePage(pagination) {
    return async dispatch => {
        dispatch({ type: HISTORY_PAGINATION_CHANGE, payload: pagination });
    }
}