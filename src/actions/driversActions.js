import { DRIVERS_PAGINATION_CHANGE, HISTORY_PAGINATION_CHANGE } from "./paginationAction";

export const GET_DRIVERS_REQUEST = 'GET_DRIVERS_REQUEST';
export const GET_DRIVERS_SUCCESS = 'GET_DRIVERS_SUCCESS';
export const GET_DRIVERS_DETAIL_REQUEST = 'GET_DRIVERS_DETAIL_REQUEST';
export const GET_DRIVERS_DETAIL_SUCCESS = 'GET_DRIVERS_DETAIL_SUCCESS';
export const POST_DRIVERS_CREATE = 'POST_DRIVERS_CREATE';
export const POST_DRIVERS_CREATE_SUCCESS = 'POST_DRIVERS_CREATE_SUCCESS';
export const POST_DRIVERS_CHANGE_BALANCE = 'POST_DRIVERS_CHANGE_BALANCE';
export const POST_DRIVERS_CHANGE_BALANCE_SUCCESS = 'POST_DRIVERS_CHANGE_BALANCE_SUCCESS';
export const GET_DRIVERS_HISTORY = 'GET_DRIVERS_HISTORY';
export const GET_DRIVERS_HISTORY_SUCCESS = 'GET_DRIVERS_HISTORY_SUCCESS';
export const PUT_DRIVERS_CHANGE = 'PUT_DRIVERS_CHANGE';
export const PUT_DRIVERS_CHANGE_SUCCESS = 'PUT_DRIVERS_CHANGE_SUCCESS';

export function getDrivers(page = 0, limit = 0) {

    const url = `https://zaqwe.info/api/drivers?page=${page}&limit=${limit}`;
    const ecodedAdmin = window.btoa('admin:qwerty');
    return dispatch => {
        dispatch({ type: GET_DRIVERS_REQUEST, payload: [] });
        fetch(`${url}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': `Basic ${ecodedAdmin}`,
            },
            responseType: 'json',
        }).then((res) => {
            console.log(res.status);
            if (res.status === 500) {
                alert('Ошибка сервера');
            }
            return res.json();
        })
            .then((response) => {
                const { drivers, pagination } = response;
                dispatch({
                    type: GET_DRIVERS_SUCCESS,
                    payload: {
                        driversList: drivers,
                        pagination: pagination
                    }
                })
                dispatch({
                    type: DRIVERS_PAGINATION_CHANGE,
                    payload: pagination
                })

            })
    }
}


export function getDriversDetail(id) {

    const url = `https://zaqwe.info/api/drivers/${id}`;
    const ecodedAdmin = window.btoa('admin:qwerty');

    return dispatch => {
        dispatch({ type: GET_DRIVERS_DETAIL_REQUEST, payload: {} });
        fetch(`${url}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': `Basic ${ecodedAdmin}`,
            },
            responseType: 'json',
        }).then((res) => res.json())
            .then((response) => {
                console.log(response);
                dispatch({
                    type: GET_DRIVERS_DETAIL_SUCCESS,
                    payload: response,
                })

            })
    }
}


export function createDriver(driver) {

    const url = `https://zaqwe.info/api/drivers`;
    const ecodedAdmin = window.btoa('admin:qwerty');

    return dispatch => {
        dispatch({ type: POST_DRIVERS_CREATE, payload: {} });
        fetch(`${url}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': `Basic ${ecodedAdmin}`,
            },
            body: JSON.stringify(driver),
            responseType: 'json',
        }).then((res) => {
            if (res.status === 200) {
                alert('Водитель успешно создан');
            }
            if (res.status === 400) {
                alert('Заполните все обязательные поля');
            }
            if (res.status === 404) {
                alert('некорректный номер телефона');
            }
            if (res.status === 500) {
                alert('Ошибка сервера');
            }
            return res.json();
        })
            .then((response) => {
                dispatch({
                    type: POST_DRIVERS_CREATE_SUCCESS,
                    payload: {
                        id: response.id,
                    }
                })
            })
    }
}


export function changeDriversBalance(id, amount, comment) {

    const url = `https://zaqwe.info/api/drivers/${id}/balance`;
    const ecodedAdmin = window.btoa('admin:qwerty');

    return async dispatch => {

        dispatch({ type: POST_DRIVERS_CHANGE_BALANCE, payload: {} });
        fetch(`${url}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': `Basic ${ecodedAdmin}`,
            },
            body: JSON.stringify({ amount, comment }),
            responseType: 'json',
        }).then((res) => {
            if (res.status === 200) {
                alert('Баланс изменён')
            }
            if (res.status === 400) {
                alert('Заполните обязательные поля')
            }
            if (res.status === 500) {
                alert('Ошибка сервера')
            }
            return res.json()

        });
        dispatch({ type: POST_DRIVERS_CHANGE_BALANCE_SUCCESS })
    }
}

export function getDriversHistory(page, limit, id) {

    const url = `https://zaqwe.info/api/drivers/${id}/history?limit=${limit}&page=${page}`;
    const ecodedAdmin = window.btoa('admin:qwerty');
    return dispatch => {
        dispatch({ type: GET_DRIVERS_HISTORY, payload: [] });
        fetch(`${url}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': `Basic ${ecodedAdmin}`,
            },
            responseType: 'json',
        }).then((res) => {
            if (res.status === 400) {
                alert('пользователя с таким id не существует');
            }
            if (res.status === 500) {
                alert('Ошибка сервера');
            }
            return res.json()
        })
            .then(response => {
                const { balanceHistories, pagination } = response;
                if (balanceHistories) {
                    dispatch({
                        type: GET_DRIVERS_HISTORY_SUCCESS,
                        payload: balanceHistories,
                    })
                    dispatch({
                        type: HISTORY_PAGINATION_CHANGE,
                        payload: pagination,
                    })
                }
            })
    }
}


export function changeDriver(id, driver) {

    const url = `https://zaqwe.info/api/drivers/${id}`;
    const ecodedAdmin = window.btoa('admin:qwerty');

    return dispatch => {
        dispatch({ type: PUT_DRIVERS_CHANGE });
        fetch(`${url}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': `Basic ${ecodedAdmin}`,
            },
            body: JSON.stringify(driver),
            responseType: 'json',
        }).then((res) => {
            if (res.status === 200) {
                alert('Данные обновлены');
            } else {
                alert('Ошибка сервера');
            }
            return res.json()
        })
        dispatch({ type: PUT_DRIVERS_CHANGE_SUCCESS })
    }
}
